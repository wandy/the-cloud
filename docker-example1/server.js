const app = require('express')();

console.log('starting up server..');

app.get('/', (req, res) => {
  res.status(200).send('Your server example1 is up!');
});

const port = process.env.PORT || 3000;

app.listen(port, '0.0.0.0', () => {
  console.log('server started at port ' + port);
});
