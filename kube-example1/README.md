# HOW TO CONNECT TO KUBERNETES

## Requirements
- Install [Docker](https://docs.docker.com/get-docker/)
- Install [kubectl](https://kubernetes.io/id/docs/tasks/tools/install-kubectl/)
- Retrieve your kubeconfig yaml file (by using Cloud Provider CLI or download the file)
- Install [Lens](https://k8slens.dev/) for better observability of your Kubernetes Cluster

## Setup
If you are using a downloaded kubeconfig file, specify the location of your kubeconfig on your Terminal / Command Prompt:

**Windows**
```
set KUBECONFIG=[YOUR KUBECONFIG LOCATION]
```

**Unix**
```
export KUBECONFIG=[YOUR KUBECONFIG LOCATION]
```

To setup Lens after getting installed, you may add a new cluster connection and choose Paste As Text -> paste the whole content of the kubeconfig.yaml

## How To Deploy
- You must pushed your example1 docker image first, please prepare your account at [Docker Hub account](https://hub.docker.com/signup) and be logged in (`docker login`), after that, you can simply run `npm run docker:push` command inside `docker-example1` directory (please verify first the command of `docker:push` inside `docker-example1/package.json`)
- After your image pushed then you may be able to run `kubectl apply -f example1-deployment.yaml`, `kubectl apply -f example1-service.yaml`, and `kubectl apply -f example1-ingress.yaml` (please verify first the location of your image inside `kube-example1/example1-deployment.yaml`)