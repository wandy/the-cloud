const app = require('express')();
const axios = require('axios');

console.log('starting up server..');

app.get('/', (req, res) => {
  res.status(200).send('Your server example2 is up!');
});

const port = process.env.PORT || 3001;

axios.get('http://example1:4001').then(response => console.log('response from server example1: ' + response.data));

app.listen(port, '0.0.0.0', () => {
  console.log('server started at port ' + port);
});
